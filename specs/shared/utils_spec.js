describe("Utils", () => {
    describe("#arrayGetLastElement", () => {
        it("should give me the last element of an array of three integers", function () {
            expect(Utils.arrayGetLastElement([1, 2, 3])).toBe(3);
        });

        it("should give me undefined for an empty array", function () {
            expect(Utils.arrayGetLastElement([])).toBeUndefined();
        });
    });

    describe("#arrayPrimitiveCompact", () => {
        it("should remove duplicates in arrays of integers", function () {
            expect(Utils.arrayPrimitiveCompact([1, 2, 3, 1])).toEqual([1, 2, 3]);
        });

        it("should remove duplicates in arrays of strings", function () {
            expect(Utils.arrayPrimitiveCompact(['aa', 'bee', 'cee', 'aa'])).toEqual(['aa', 'bee', 'cee']);
        });
    });
});