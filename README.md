# LinkedPwn 0.0.1

LinkedPwn is a hacked together scraper packaged as a Chrome extension that allows you to maintain an exportable local copy of all your LinkedIn data.

## Development Setup

* Check out this repo
* Open chrome://extensions/
* Enable the "Developer Mode" toggle
* Click on "Load Unpacked" and open the directory `repo-root/extension`
* To run jasmine specs in your default browser, run `make specs`


