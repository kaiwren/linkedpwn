L.i("Loading content.js...");

document.onreadystatechange = function () {
    if (document.readyState === "complete") {
        if (location.href === "https://www.linkedin.com/mynetwork/") {
            console.log("On /mynetwork");
            Message.initialize();

            let connectionsCount = document.getElementsByClassName("mn-connections-summary__count");
            new Message(Message.types.totalConnectionsCount)
                .setConnectionsCount(parseInt(connectionsCount[0].textContent.replace(',', '')))
                .send();
        }
        else if (location.href.startsWith("https://www.linkedin.com/in/")) {
            L.i("On /in");
            new ConnectionPage().forceWholePageToLoad();
        }
        else if (location.href.startsWith("https://www.linkedin.com/search/results/people/")) {
            console.log("On /search/results/people");
            new SearchPage().forceWholePageToLoad();
        }
    }
};

L.i("Loaded content.js");


// Hack - If feed page, reload extension
if (location.href.startsWith("https://www.linkedin.com/feed/")) {
    Message.initialize();
    new Message(Message.types.requestReload).send();
}
