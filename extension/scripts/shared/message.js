class Message {
    constructor(type, payload, sender) {
        this.type = type;
        this.payload = payload;
        this.sender = sender;

        if (this.payload === undefined || this.payload === null) {
            this.payload = {};
        }
    }

    static get handlers() {
        return this._handlers;
    }

    static get types() {
        return this._types;
    }

    static initialize() {
        this._handlers = {};
        this._types = {
            totalConnectionsCount: 'totalConnectionsCount',
            requestTotalConnectionsCount: 'requestTotalConnectionsCount',

            connectionsSummaries: 'connectionsSummaries',
            requestConnectionsSummaries: 'requestConnectionsSummaries',

            connectionDetail: 'connectionDetail',
            requestConnectionsDetails: 'requestConnectionsDetails',

            tabId: 'tabId',
            requestTabId: 'requestTabId',

            requestReload: 'requestReload',
            requestChangeWindowLocation: 'requestChangeWindowLocation'
        };

        // Register with chrome runtime event bus
        chrome.runtime.onMessage.addListener((message, sender) => {
            new Message(message.type, message.payload, sender).dispatch();
        });

        // Configure TabId request handler so tabs can ask for their numbers
        Message.registerHandler(Message.types.requestTabId, (message) => {
            let tabId = message.sender.tab.id;
            if (tabId) {
                new Message(Message.types.tabId, {tabId: tabId}).sendTo(tabId);
            }
        });
        L.i("Message: Initialized Environment");
        return this;
    }

    static registerHandler(messageType, handler) {
        this._handlers[messageType] = handler;
        return this;
    }

    static registerChangeWindowLocationHandler() {
        return Message.registerHandler(Message.types.requestChangeWindowLocation, (message) => {
            L.w("Window: Location " + message.payload.location);
            window.location.href = message.payload.location;
        });
    }

    static nullResponseHandler() {
        return (object) => {
            if (object) {
                L.d("Null Handler: ", object);
            }
        }
    }

    setConnectionsCount(connectionsCount) {
        this.payload.totalConnectionsCount = connectionsCount;
        return this;
    }

    send() {
        let self = this;
        L.i("Message: send", self);
        chrome.runtime.sendMessage({
            type: self.type,
            payload: self.payload
        }, Message.nullResponseHandler());
    }

    sendTo(tabId) {
        let self = this;
        L.i("Message: sendTo " + tabId, self);
        chrome.tabs.sendMessage(tabId, {
            type: self.type,
            payload: self.payload
        }, Message.nullResponseHandler());
    }

    sendToActiveTab() {
        let self = this;

        chrome.tabs.query(
            {active: true, currentWindow: true},
            (tabs) => {
                if (tabs.length < 1) {
                    L.e("couldn't find active tab");
                }
                else {
                    let id = tabs[0].id;
                    L.i("Message: sendToActiveTab " + id, self);
                    self.sendTo(id);
                }
            }
        );
    }

    /**
     * Used in Message.initialize() to handle
     * incoming message dispatch. Not for public
     * use.
     */
    dispatch() {
        let self = this;

        self.logIncomingMessage();

        let handler = Message.handlers[self.type];
        if (!handler) {
            handler = Message.nullResponseHandler()
        }
        L.i("Message: Handling " + self.type);
        handler(self);
    }

    logIncomingMessage() {
        let self = this;
        if (self.sender) {
            if (self.sender.tab) {
                L.i("Message: From " + self.sender.tab.id, self);
            }
            else {
                L.d("Message: ", self, " From ", self.sender);
            }
        }
        else {
            L.d("Message: ", self);
        }
    }
}
