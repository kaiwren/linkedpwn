class Connection {
    constructor(attributes) {
        this.db = new DB();
        this.attributes = attributes;
    }

    static buildMessagePayloadFromUrl(urls) {
        return urls.map((url) => {
            let connection = {};
            connection.url = url;
            return connection;
        });
    }

    static buildMessagePayloadFromSearchResultELements(searchResultWrapperElements) {
        L.d("Search Result Elements: ", searchResultWrapperElements);
        let connectionsAttributes = Array.from(searchResultWrapperElements).map((resultElement) => {
            L.d("Result Element: ", resultElement);
            let connection = {};
            connection.url = resultElement.getElementsByClassName('search-result__result-link ember-view')[0].href;
            connection.name = resultElement.getElementsByClassName('name actor-name')[0].innerHTML;
            return connection;
        });
        L.d("Connection: Attributes", connectionsAttributes);
        return connectionsAttributes;
    }

    upsert() {
        let self = this;
        let connectionUrl = self.attributes.url;
        let connectionPath = connectionUrl.replace(
            'https://www.linkedin.com',
            ''
        );
        self.db.upsert("connection|" + connectionPath,
            (doc) => {
                self.attributes = Object.assign(self.attributes, doc);
                self.attributes.updatedAt = Utils.unixTimestampNow();
                L.d("Connection: Upserting ", self.attributes);
                return self.attributes;
            });
    }
}
