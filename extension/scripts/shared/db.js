class DB {
    constructor() {
        this.initialize();
    }

    initialize() {
        this.db = new PouchDB("linkedin");
    }

    get(id) {
        return this.db.get(id);
    }

    upsert(id, handler) {
        return this.db.upsert(id, handler)
            .then((res) => {
                L.d("DB: Upsert", res);
            }).catch((err) => {
                L.e("DB: Upsert Error", err);
            });
    }

    put(doc) {
        let self = this;
        doc.updatedAt = Utils.unixTimestampNow();

        return this.db.put(doc, (error, result) => {
            if (!error) {
                L.d("DB: Saved ", result);
            }
            else {
                L.w("DB: Saving ", error);
            }
        });
    }

    putIfNotExists(doc) {
        let self = this;
        doc.updatedAt = Utils.unixTimestampNow();

        self.db.putIfNotExists(doc._id, doc).then((result) => {
            L.i("DB: PutIfNotExists", result);
        }).catch(function (error) {
            L.e("DB: PutIfNotExists failed", error);
        });
    }

    putConnectionIfNotExists(connectionUrl) {
        let self = this;
        let connectionPath = connectionUrl.replace(
            'https://www.linkedin.com',
            ''
        );
        self.putIfNotExists({
            _id: "connection|" + connectionPath,
            url: connectionUrl
        });
    }

    destroyAndCreate() {
        let self = this;

        this.db.destroy().then(
            (response) => {
                L.i("DB: Destroyed");
                self.initialize();
                L.i("DB: Created");
            }).catch((error) => {
                L.e("DB: Error", error);
            }
        );
    }

    /**
     * Load all saved LinkedIn Connection Docs
     * and pass them to <code>handler</code>
     * @param {Function} handler
     */
    loadConnections(handler) {
        let self = this;

        self.db.allDocs({
            include_docs: true,
            startkey: 'connection|',
            endkey: 'connection|\ufff0'
        }).then(function (result) {
            L.d("DB: Load connections", result);
            handler(result);
        }).catch(function (error) {
            L.e("DB: Load connections failed ", error)
        });
    }

    downloadAllAsJson() {
        let self = this;
        let json = {
            connectionsCount: 0,
            connections: []
        };

        L.i("DB: Starting download");
        self.loadConnections((results) => {
            for (let row of results.rows) {
                json.connections.push(row.doc);
            }

            let blob = new Blob([JSON.stringify(json, null, 2)], {type: "application/json"});
            let url = URL.createObjectURL(blob);
            chrome.downloads.download({
                url: url,
                filename: "dbdump-" + Date.now() + ".json"
            });
        });
    }

    changes(options) {
        return this.db.changes(options);
    }
}
