class Utils {
    static elementsSortByHeight(arrayOfElements) {
        return arrayOfElements.sort(function (a, b) {
            let aBottom = a.getBoundingClientRect().bottom;
            let bBottom = b.getBoundingClientRect().bottom;
            if (aBottom < bBottom) return -1;
            if (aBottom > bBottom) return 1;
            return 0;
        });
    }

    /**
     * The last element in an array
     * @param {Array} array
     * @return {Object} array's last element
     */
    static arrayGetLastElement(array) {
        return array[array.length - 1];
    }

    /**
     * Removes duplicates from an array of primitives
     * @param {Array} array
     * @return {Array} a unique set of objects
     */
    static arrayPrimitiveCompact(array) {
        return Array.from(
            new Set(array)
        );
    }

    /**
     * Removes duplicates from an array of objects
     * @param {Array} array
     * @param {String} fieldToCompactOn based which
     *        to do equality check for compaction
     * @return {Array} a unique set of objects
     */
    static arrayObjectCompact(array, fieldToCompactOn) {
        let indexedArray = {};
        for (let item of array) {
            indexedArray[item[fieldToCompactOn]] = item;
        }
        return Array.from(Object.values(indexedArray));
    }

    static unixTimestampNow() {
        return Math.round(new Date().getTime() / 1000);
    }
}
