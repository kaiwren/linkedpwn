class PubSub {
    constructor() {
        this._handlers = [];
    }

    /**
     * Subscribe to messages
     * <code>
     *     this.pubsub.subscribe('message', this.emitMessage, this);
     * </code>
     * @param {String} eventName
     * @param {Function} handler
     * @param {Object} binding
     */
    subscribe(eventName, handler, binding) {
        if (typeof binding === 'undefined') {
            binding = handler;
        }
        this._handlers.push({event: eventName, handler: handler.bind(binding)});
    }

    /**
     * <code>
     *     this.pubsub.publish('message', 'Hey, how are you?');
     * </code>
     * @param {String} eventName
     * @param {Object} eventData
     */
    publish(eventName, eventData) {
        this._handlers.forEach(topic => {
            if (topic.event === eventName) {
                topic.handler(eventData)
            }
        })
    }
}
