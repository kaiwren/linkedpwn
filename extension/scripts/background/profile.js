class Profile {
    constructor(db) {
        this.db = db;
        this.connectionsUrls = [];
        this.page = 1;
        this.totalConnectionsCount = 0;
        this.connectionsPerPage = 10;
        this.connectionsPaginateUrl = "https://www.linkedin.com/search/results/people/?facetNetwork=%5B%22F%22%5D&origin=FACETED_SEARCH";
        this.connectionsCountUrl = "https://www.linkedin.com/mynetwork/";

        this.observeDb();
    }

    observeDb() {
        let self = this;

        self.db.changes({
            since: 'now',
            live: true
        }).on('change', (event) => {
        });
    }

    setTotalConnectionsCount(connectionsCount) {
        let self = this;

        self.totalConnectionsCount = connectionsCount;
        L.d("Profile: PutIfNotExists Total Connections Count " + self.totalConnectionsCount);
        return self.db.upsert(
            'profile|connectionCount',
            (doc) => {
                if (doc.count !== connectionsCount) {
                    doc.count = connectionsCount;
                    doc.updatedAt = Utils.unixTimestampNow();
                    return doc;
                }
                // Don't update the doc
                return false;
            }
        );
    }

    /**
     * Load total connections count
     * @param {Function} callback which accepts
     * an integer count as its only argument
     */
    loadTotalConnectionsCount(handler) {
        let self = this;

        self.db.get('profile|connectionCount').then(function (doc) {
            L.d("Profile: Get Total Connections Count " + doc.count);
            handler(doc.count);
        }).catch((err) => {
            L.e("Profile: Get Total Connections Count ", err);
        });
    }

    incrementPageNumber() {
        let self = this;

        self.page++;
        self.db.upsert('profile|pageNumber', (doc) => {
            doc.pageNumber = self.page;
            return doc;
        });
    }

    addUniqueConnections(connections) {
        let self = this;
        self.connectionsUrls =
            Utils.arrayPrimitiveCompact(
                self.connectionsUrls.concat(connections.map(connection => connection.url))
            );
        L.d("Profile: Added Unique Connections", self.connectionsUrls);
        for (let connectionAttributes of connections) {
            new Connection(connectionAttributes).upsert();
        }
    }

    maybeGoToNextSearchResultsTab(tabId) {
        let self = this;

        if (self.atPageBoundary()) {
            self.incrementPageNumber();
            new Message(
                Message.types.requestChangeWindowLocation,
                {
                    location: self.connectionsPaginateUrlForCurrentPage(),
                }
            ).sendTo(tabId);
        }
    }

    maybeCloseMyNetworkTab(tabId) {
        chrome.tabs.remove(tabId, function () {
            L.i("Tab: Closed MyNetwork " + tabId);
        });
    }

    openSearchResultsTab() {
        chrome.tabs.create({url: currentUser.connectionsPaginateUrlForCurrentPage()});
    }

    openMyNetworkTab() {
        chrome.tabs.create({url: "https://www.linkedin.com/mynetwork/"});
    }

    atPageBoundary() {
        let connectionsCount = this.connectionsUrls.length;
        let isAtPageBoundary = (connectionsCount % this.connectionsPerPage) === 0;
        L.i("Profile: Connections Count " + connectionsCount);
        if (isAtPageBoundary) {
            L.i("Profile: At Page Boundary");
        }
        return isAtPageBoundary;
    }

    connectionsPaginateUrlForCurrentPage() {
        return this.connectionsPaginateUrl + "&page=" + this.page;
    }

    openConnectionDetailPage() {
        let self = this;
        self.db.loadConnections((results) => {
            let url = results.rows[0].doc.url;
            L.w("Profile: Opening Connection Details Page in a new Tab");
            chrome.tabs.create({url: url});
        });
    }
}
