let currentdate = new Date();
let datetime = "Last Sync: " + currentdate.getDate() + "/"
    + (currentdate.getMonth() + 1) + "/"
    + currentdate.getFullYear() + " @ "
    + currentdate.getHours() + ":"
    + currentdate.getMinutes() + ":"
    + currentdate.getSeconds();

var L = {
    l: console.log,
    d: console.debug,
    i: console.info,
    w: console.warn,
    e: console.error
};

// Log boot up timestamp
L.i(datetime);

// Check for dependencies
if (PouchDB) {
    L.i("PouchDB Loaded");
}
else {
    L.e("PouchDB Not Found");
}
