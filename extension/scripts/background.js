L.l("Load background.js...");


let db = new DB();
let currentUser = new Profile(db);

L.i("Loaded background.js");

Message
    .initialize()
    .registerHandler(Message.types.requestTotalConnectionsCount, (message) => {
        L.i("Request Total Connections Count...");
        currentUser.openMyNetworkTab();
    })
    .registerHandler(Message.types.totalConnectionsCount, (message) => {
        L.i("Incoming Total Connections Count...");
        currentUser.setTotalConnectionsCount(message.payload.totalConnectionsCount);
        currentUser.loadTotalConnectionsCount((count) => {
            L.i("Loaded Connections Count: " + count)
        });
    })
    .registerHandler(Message.types.requestConnectionsSummaries, (message) => {
        L.i("Request Connections Summaries...");
        currentUser.openSearchResultsTab();
    })
    .registerHandler(Message.types.connectionsSummaries, (message) => {
        L.i("Incoming Connections Summaries...");
        currentUser.addUniqueConnections(message.payload);
//        currentUser.maybeGoToNextSearchResultsTab(message.sender.tab.id);
    })
    .registerHandler(Message.types.requestConnectionsDetails, (message) => {
        L.i("Request Connections Details...");
        currentUser.openConnectionDetailPage();
    })
    .registerHandler(Message.types.connectionsDetails, (message) => {
        L.i("Incoming Connections Details...", message);
    })
    .registerHandler(Message.types.requestReload, (message) => {
        L.i("Request Reload...");
        chrome.runtime.reload();
    });