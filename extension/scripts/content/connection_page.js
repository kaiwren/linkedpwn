class ConnectionPage {
    static get ClassNames() {
        if (!this._classNames) {
            this._classNames = {
                SkillsShowMoreButtonElement: 'pv-profile-section__card-action-bar pv-skills-section__additional-skills artdeco-container-card-action-bar',
                ProfileDetailDivElement: 'profile-detail'
            };
        }
        return this._classNames;
    }

    constructor() {
        this.hrefStartsWith = "https://www.linkedin.com/in/";
        this.initialize();
    }

    initialize() {
        let self = this;
        Message
            .initialize()
            .registerChangeWindowLocationHandler();
        self.profileDetailDivElement = document.getElementsByClassName(
            ConnectionPage.ClassNames.ProfileDetailDivElement
        )[0];
        L.i("ConnectionPage: ProfileDetailDivElement", self.profileDetailDivElement);
        self.initializeConnectionDetailsDOMObserver();
        self.initializeWindowScrollObserver();
    }

    initializeConnectionDetailsDOMObserver() {
        let self = this;

        self.observer = new MutationObserver(function (mutationsList) {
            for (let mutation of mutationsList) {
                if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                    let addedDivWrapperNodes = Array
                        .from(mutation.addedNodes)
                        .filter((node) => {
                            return node.className === ConnectionPage.ClassNames.SkillsShowMoreButtonElement;
                        });
                    if (addedDivWrapperNodes.length > 0) {
                        L.i("DOM Updated with new results", addedDivWrapperNodes);
                        addedDivWrapperNodes.forEach(node => node.click());
                        self.fullPageIsLoaded = true;
                    }
                }
            }
        });

        self.observer.observe(
            self.profileDetailDivElement,
            {subtree: true, childList: true}
        );
    }

    initializeWindowScrollObserver() {
        let self = this;

        window.addEventListener('scroll', function () {
            L.d("Responding to scroll, y " + window.scrollY);


            if (self.isSkillsSectionLoaded()) {
                if (self.isFullPageLoaded()) {
                    new Message(
                        Message.types.connectionDetail,
                        self.loadConnectionAttributes()
                    ).send();
                    return;
                }
            }
            else {
                self.forceWholePageToLoad();
            }
            L.d("End responding to scroll, y " + window.scrollY);
        });
    }

    isFullPageLoaded() {
        L.d("ConnectionPage: IsFullPageLoaded " + this.fullPageIsLoaded);
        return this.fullPageLoadedIsLoaded;
    }


    forceWholePageToLoad() {
        let yPlus = 30;
        window.scrollBy(0, yPlus);
        L.d("Scrolling by: " + yPlus);
    }

    loadConnectionAttributes() {
        return {};
    }

    isSkillsSectionLoaded() {
        let skillsShowMoreElementRendered = this.loadSkillsShowMoreElements().length > 0;
        L.d("ConnectionPage: isSkillsSectionLoaded " + skillsShowMoreElementRendered);
        return skillsShowMoreElementRendered;
    }

    loadSkillsShowMoreElements() {
        return this.skillsShowMoreElements = Array.from(
            document.getElementsByClassName(ConnectionPage.ClassNames.SkillsShowMoreButtonElement)
        );
    }
}
