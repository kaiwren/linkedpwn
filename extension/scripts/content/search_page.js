class SearchPage {
    static get ClassNames() {
        if (!this._classNames) {
            this._classNames = {
                SearchResultAnchorWithUrl: 'search-result__result-link',
                SearchResultH3WithName: 'name-and-icon',
                SearchResultDivWrapper: 'search-result__wrapper',
                SearchResultsPaginationElement: 'page-list'
            };
        }
        return this._classNames;
    }

    constructor() {
        this.searchResultsPageElement = document.getElementsByClassName(
            "search-results-page core-rail"
        )[0];

        this.hrefStartsWith = "https://www.linkedin.com/search/results/people/";
        this.expectedSearchResultsPerPage = 10;
        this.initialize();
    }

    initialize() {
        let self = this;

        self.validate();
        L.d("Search results element", self.searchResultsPageElement);
        self.initializeMessageEnvironmentAndParser();
        self.initializeSearchResultsDOMObserver();
        self.initializeWindowScrollObserver();
    }

    validate() {
        let self = this;

        if (!self.searchResultsPageElement) {
            L.e("Fatal Error! No search results DOM element found.")
        }
    }

    initializeMessageEnvironmentAndParser() {
        let self = this;

        Message.initialize()
            .registerHandler(Message.types.requestConnectionsSummaries, (message) => {
                self.forceWholePageToLoad();
            })
            .registerHandler(Message.types.requestChangeWindowLocation, (message) => {
                L.w("Window: Location " + message.payload.location);
                window.location.href = message.payload.location;
            })
            .registerHandler(Message.types.tabId, (message) => {
                L.i("Tab Id ", message.payload.tabId);
            });
        new Message(Message.types.requestTabId).send();
    }

    isFullPageLoaded() {
        let isSearchResultPaginationElementRendered = document.getElementsByClassName(SearchPage.ClassNames.SearchResultsPaginationElement).length > 0;
        let searchResultsElements = this.reloadSearchResultsElements();
        let searchResultElementsCount = searchResultsElements.length;
        let areAllSearchResultElementsRendered = searchResultElementsCount === this.expectedSearchResultsPerPage;
        let isFullPageLoaded = isSearchResultPaginationElementRendered && areAllSearchResultElementsRendered;
        L.d("SearchPage: IsFullPageLoaded " + isFullPageLoaded);
        return isFullPageLoaded;
    }

    initializeWindowScrollObserver() {
        let self = this;

        window.addEventListener('scroll', function () {
            L.d("Responding to scroll, y " + window.scrollY);

            if (self.isFullPageLoaded()) {
                new Message(
                    Message.types.connectionsSummaries,
                    self.reloadSearchResultsElementsAndMapToConnectionAttributes()
                ).send();
                return;
            }
            self.forceWholePageToLoad();
            L.d("End responding to scroll, y " + window.scrollY);
        });
    }


    initializeSearchResultsDOMObserver() {
        let self = this;

        self.observer = new MutationObserver(function (mutationsList) {
            for (let mutation of mutationsList) {
                if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                    let addedDivWrapperNodes = Array
                        .from(mutation.addedNodes)
                        .filter((node) => {
                            return node.className === SearchPage.ClassNames.SearchResultDivWrapper;
                        });
                    if (addedDivWrapperNodes.length > 0) {
                        L.d("DOM Updated with new results", addedDivWrapperNodes);
                        new Message(
                            Message.types.connectionsSummaries,
                            Connection.buildMessagePayloadFromSearchResultELements(addedDivWrapperNodes)
                        ).send();
                    }
                }
            }
        });

        self.observer.observe(
            self.searchResultsPageElement,
            {subtree: true, childList: true}
        );
    }

    reloadSearchResultsElementsAndMapToConnectionAttributes() {
        return Connection.buildMessagePayloadFromSearchResultELements(
            this.reloadSearchResultsElements()
        );
    }

    reloadSearchResultsElements() {
        return Array.of(
            document.getElementsByClassName(
                SearchPage.ClassNames.SearchResultDivWrapper
            )
        )[0];
    }

    forceWholePageToLoad() {
        let self = this;
        let yPlus = 30;
        window.scrollBy(0, yPlus);
        L.d("Scrolling by: " + yPlus);
    }
}
