L.i("Load popup.js...");

Message.initialize();

document.addEventListener('DOMContentLoaded', function () {
    let updateConnectionsAnchor = document.getElementById('updateConnectionsAnchor');
    let updateConnectionsDetailsAnchor = document.getElementById('updateConnectionsDetailsAnchor');
    let updateConnectionsCountAnchor = document.getElementById('updateConnectionsCountAnchor');
    let downloadAnchor = document.getElementById('downloadAnchor');
    let requestReloadAnchor = document.getElementById('requestReloadAnchor');
    let connectionsCountSpan = document.getElementById('countSpan');
    let db = new DB();

    updateConnectionsAnchor.addEventListener('click', (event) => {
        new Message(Message.types.requestConnectionsSummaries).send();
    });

    updateConnectionsCountAnchor.addEventListener('click', (event) => {
        new Message(Message.types.requestTotalConnectionsCount).send();
    });

    updateConnectionsDetailsAnchor.addEventListener('click', (event) => {
        new Message(Message.types.requestConnectionsDetails).send();
    });

    downloadAnchor.addEventListener('click', (event) => {
        db.downloadAllAsJson();
    });

    requestReloadAnchor.addEventListener('click', (event) => {
        new Message(Message.types.requestReload).send();
    });
});

L.i("Loaded popup.js");